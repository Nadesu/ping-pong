﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public float MoveSpeed;
    public float ExtraSpeedPerHit;
    public float maxExtraSpeed;

    int hitCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(this.StartBall());
    }

    private void positionBall(bool isStartingPlayer1)
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        if (isStartingPlayer1)
        {
            this.gameObject.transform.localPosition = new Vector3(-100, 0, 0);
        }
        else
        {
            this.gameObject.transform.localPosition = new Vector3(100, 0, 0);
        }


    }

    public IEnumerator StartBall(bool isStartingPlayer1 = true)
    {
        this.positionBall(isStartingPlayer1);
        this.hitCounter = 0;
        yield return new WaitForSeconds(2);
        if (isStartingPlayer1)
        {
            this.MoveBall(new Vector2(-1, 0));
        }
        else
        {
            this.MoveBall(new Vector2(1, 0));
        }

    }
    public void MoveBall(Vector2 dir)
    {
        dir = dir.normalized;

        float speed = this.MoveSpeed + this.hitCounter * this.ExtraSpeedPerHit;

        Rigidbody2D rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
        rigidbody2D.velocity = dir * speed;

    }


    public void IncreaseHitCounter()
    {
        if (this.hitCounter * this.ExtraSpeedPerHit <= this.maxExtraSpeed)
        {
            this.hitCounter++;
        }

    }

}
