﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreController : MonoBehaviour
{
    private int scorePlayer1 = 0;
    private int scorePlayer2 = 0;

    public GameObject ScorePlayer1;
    public GameObject ScorePlayer2;

    public int GoalToWin;

    public void Update()
    {
        if (this.scorePlayer1 >= this.GoalToWin || this.scorePlayer2 >= this.GoalToWin)
        {
            Debug.Log("Game won");
            SceneManager.LoadScene("GameOver");
        }
    }

    private void FixedUpdate()
    {
        Text uiScorePlayer1 = this.ScorePlayer1.GetComponent<Text>();
        uiScorePlayer1.text = this.scorePlayer1.ToString();

        Text uiScorePlayer2 = this.ScorePlayer2.GetComponent<Text>();
        uiScorePlayer2.text = this.scorePlayer2.ToString();
    }

    public void GoalPlayer1()
    {
        scorePlayer1++;
    }
    public void GoalPlayer2()
    {
        scorePlayer2++;
    }

}
